﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStore.Domain.Entities;

namespace WebStore.WebUI.Helpers
{
    public class PriceHelper
    {
        public static decimal Total(IEnumerable<OrderLine> orderLines)
        {
            decimal total = 0;
            foreach (OrderLine ol in orderLines)
            {
                total += ol.Product.Price * ol.Quantity;
            }
            return total;
        }
    }
}