﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebStore.WebUI.Infrastructure.Abstract;
using WebStore.WebUI.Models;
using WebStore.Domain.Entities.Authorization;
using WebStore.WebUI.Providers;
using WebStore.Domain.Abstract;
using System.Collections;


namespace WebStore.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private IUserRepository repository;

        public AccountController(IUserRepository repo)
        {
            repository = repo;
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Email, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("List", "Product");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный пароль или логин");
                }
            }
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult LogOff(string returnUrl)
        {
            FormsAuthentication.SignOut();
            if (Url.IsLocalUrl(returnUrl))
            
                return Redirect(returnUrl);
                return RedirectToAction("List", "Product");
            

            //return RedirectToAction("List", "Product");
        }
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                MembershipUser membershipUser = ((WebStoreMembershipProvider)Membership.Provider).CreateUser(model.Email, model.Password, model.FirstName, model.SecondName);

                if (membershipUser != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    return RedirectToAction("List", "Product");
                }
                else
                {
                    ModelState.AddModelError("", "Error registration. This email is already occupied by someone else!");
                }
            }
            return View(model);
        }

        [Authorize]
        public ActionResult Manage()
        {
            User user = repository.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            IEnumerable<User> data = repository.Users;
            ViewBag.UserID = user.Id;
            ViewBag.Data = data;
            return View();
        }
    }
}
