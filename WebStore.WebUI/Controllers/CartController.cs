﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using WebStore.Domain.Abstract;
using WebStore.Domain.Entities;
using WebStore.WebUI.Models;
using WebStore.Domain.Entities.Authorization;
using WebStore.Domain.Concrete;

namespace WebStore.WebUI.Controllers
{
    public class CartController : Controller
    {
        private EFDbContext _db = new EFDbContext();
        private IProductRepository repository;
        private IOrdersRepository ordersRepository;
        private IUserRepository userRepository;
        public CartController(IProductRepository repo, IOrdersRepository ordersRepo, IUserRepository userRepo)
        {
            repository = repo;
            ordersRepository = ordersRepo;
            userRepository = userRepo;
        }

        public ViewResult Index(Cart cart, string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
        {
            Product product = repository.Products
            .FirstOrDefault(p => p.ProductID == productId);
            if (product != null)
            {
                cart.AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            Product product = repository.Products
            .FirstOrDefault(p => p.ProductID == productId);
            if (product != null)
            {
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Checkout()
        {
            if(User.Identity.IsAuthenticated)
            { 
            User user = userRepository.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ViewBag.UserID = user.Id;
            }
            return View(new Order());
        }

        [HttpPost]
        [Authorize]
        public ViewResult Checkout(Cart cart, Order order)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }
            if (ModelState.IsValid)
            {
                order.OrderLines = new List<OrderLine>();
                foreach (CartLine line in cart.Lines)
                {
                    order.OrderLines.Add(new OrderLine { Order = order, Product = line.Product, Quantity = line.Quantity });
                }
                ordersRepository.SaveOrder(order);
                cart.Clear();
                return View("Completed");
            }
            else
            {
                return View(order);
            }
        }
        [Authorize]
        public ActionResult UserOrder()
        {
            User user = userRepository.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            IEnumerable<Order> orders = ordersRepository.Orders.Where(o => o.UserId == user.Id);
            return View(orders);
        }
    }
}
