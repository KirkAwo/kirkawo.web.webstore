﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStore.Domain.Abstract;
using WebStore.Domain.Entities;

namespace WebStore.WebUI.Controllers
{
    [Authorize(Roles = "admin, manager")]
    public class ManagerController : Controller
    {
        private IOrdersRepository repository;

        public ManagerController(IOrdersRepository repo)
        {
            repository = repo;
        }

        public ActionResult Index()
        {
            return View(repository.Orders);
        }

        public ActionResult DeliveryEdit(int? orderId)
        {
            Order order = repository.Orders.FirstOrDefault(o => o.OrderId == orderId);
            return View(order);
        }

        [HttpPost]
        public ActionResult DeliveryEdit(Order order)
        {
            if (ModelState.IsValid)
            {
                repository.SaveOrder(order);
                TempData["messege"] = string.Format("Order {0} was considered", order.OrderId);
                return RedirectToAction("Index");
            }
            else
            {
                return View(order);
            }
        }
    }
}
