﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Moq;
using WebStore.Domain.Abstract;
using WebStore.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using WebStore.Domain.Concrete;
using System.Configuration;
using WebStore.WebUI.Infrastructure.Concrete;
using WebStore.WebUI.Infrastructure.Abstract;

namespace WebStore.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public NinjectControllerFactory()
        {
            // создание контейнера
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            // получение объекта контроллера из контейнера 
            // используя его тип
            return controllerType == null
              ? null
              : (IController)ninjectKernel.Get(controllerType);
        }
        private void AddBindings()
        {
            ninjectKernel.Bind<IProductRepository>().To<EFProductRepository>();
            ninjectKernel.Bind<IOrdersRepository>().To<EFOrderRepository>();
            ninjectKernel.Bind<IUserRepository>().To<EFUserRepository>(); //added to testing new method in CartController
            EmailSettings emailSettings = new EmailSettings //refactoring
            {
                WriteAsFile = bool.Parse(ConfigurationManager
                  .AppSettings["Email.WriteAsFile"] ?? "false")
            };
            ninjectKernel.Bind<IOrderProcessor>()//refactoring
              .To<EmailOrderProcessor>()
              .WithConstructorArgument("settings", emailSettings);
            ninjectKernel.Bind<IAuthProvider>().To<FormsAuthProvider>();//refactoring
        }
    }
}