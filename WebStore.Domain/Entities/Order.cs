﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebStore.Domain.Entities
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter a country name")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please enter a state name")]
        public string Region { get; set; }
        [Required(ErrorMessage = "Please enter a city name")]
        public string City { get; set; }
        public string CityIndex { get; set; }
        [Required(ErrorMessage = "Please enter a street name")]
        public string Street { get; set; }
        [Required(ErrorMessage = "Please enter your phone number")]
        public string PhoneNumber { get; set; }
        public int UserId { get; set; }
        public bool GiftWrap { get; set; }
        public bool? Dispatched { get; set; } //Order is shipped or not. For Manager.
        /*The order is processed = null
         *The order is considered a manager = false
         *The service delivery = true
         */
        public virtual List<OrderLine> OrderLines { get; set; }
    }

    public class OrderLine
    {
        public int OrderLineId { get; set; }
        public Order Order { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
