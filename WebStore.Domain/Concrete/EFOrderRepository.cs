﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebStore.Domain.Abstract;
using WebStore.Domain.Entities;

namespace WebStore.Domain.Concrete
{
    public class EFOrderRepository : IOrdersRepository
    {
        private EFDbContext context = new EFDbContext();
        // Reading data from the Orders table
        public IEnumerable<Order> Orders
        {
            get
            {
                return context.Orders.Include(o => o.OrderLines.Select(ol => ol.Product));
            }
        }

        // Save the order data in the database
        public void SaveOrder(Order order)
        {
            if (order.OrderId == 0)
            {
                order = context.Orders.Add(order);

                foreach (OrderLine line in order.OrderLines)
                {
                    context.Entry(line.Product).State
                        = EntityState.Modified;
                }

            }
            else
            {
                Order dbOrder = context.Orders.Find(order.OrderId);
                if (dbOrder != null)
                {
                    dbOrder.Name = order.Name;
                    dbOrder.Country = order.Country;
                    dbOrder.Region = order.Region;
                    dbOrder.City = order.City;
                    dbOrder.CityIndex = order.CityIndex;
                    dbOrder.Street = order.Street;
                    dbOrder.PhoneNumber = order.PhoneNumber;
                    dbOrder.UserId = order.UserId;
                    dbOrder.GiftWrap = order.GiftWrap;
                    dbOrder.Dispatched = order.Dispatched;
                }
            }
            context.SaveChanges();
        }
    }
}
